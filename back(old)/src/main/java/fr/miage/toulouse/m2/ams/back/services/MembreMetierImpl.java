package fr.miage.toulouse.m2.ams.back.services;

import fr.miage.toulouse.m2.ams.back.entities.*;
import fr.miage.toulouse.m2.ams.back.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MembreMetierImpl implements MembreMetier {
    @Autowired
    MembreRepository membreRepository;
    @Autowired
    SecretaireRepository secretaireRepository;
    @Autowired
    EnseignantRepository enseignantRepository;
    @Autowired
    PresidentRepository presidentRepository;

    @Override
    public Membre creerMembre(Membre m) {
        return this.membreRepository.save(m);
    }

    @Override
    public Secretaire creerSecretaire(Secretaire s) {
        return this.secretaireRepository.save(s);
    }

    @Override
    public Enseignant creerEnseignant(Enseignant e) {
        return this.enseignantRepository.save(e);
    }

    @Override
    public President creerPresident(President p) {
        return this.presidentRepository.save(p);
    }

}
