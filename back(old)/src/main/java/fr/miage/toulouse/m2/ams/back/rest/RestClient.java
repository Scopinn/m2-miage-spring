package fr.miage.toulouse.m2.ams.back.rest;

import fr.miage.toulouse.m2.ams.back.entities.Enseignant;
import fr.miage.toulouse.m2.ams.back.entities.Membre;
import fr.miage.toulouse.m2.ams.back.entities.President;
import fr.miage.toulouse.m2.ams.back.entities.Secretaire;
import fr.miage.toulouse.m2.ams.back.repositories.EnseignantRepository;
import fr.miage.toulouse.m2.ams.back.repositories.MembreRepository;
import fr.miage.toulouse.m2.ams.back.repositories.PresidentRepository;
import fr.miage.toulouse.m2.ams.back.repositories.SecretaireRepository;
import fr.miage.toulouse.m2.ams.back.services.MembreMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/membres")
public class RestClient {

    @Autowired
    MembreRepository membreRepository;

    @Autowired
    EnseignantRepository enseignantRepository;

    @Autowired
    SecretaireRepository secretaireRepository;

    @Autowired
    PresidentRepository presidentRepository;

    @Autowired
    MembreMetier membreMetier;

    @GetMapping("/{id}")
    public Membre getUnMembre(@PathVariable("id") Membre m) {
        return m;
    }

    @GetMapping("/secretaire/{id}")
    public Secretaire getUnSecretaire(@PathVariable("id") Secretaire m) {
        return m;
    }

    @GetMapping("/enseignant/{id}")
    public Enseignant getUnEnseignant(@PathVariable("id") Enseignant m) {
        return m;
    }

    @GetMapping("/president/{id}")
    public President getUnPresident(@PathVariable("id") President m) {
        return m;
    }

    @PostMapping("")
    public Membre postMembre(@RequestBody Membre m) {
        return this.membreMetier.creerMembre(m);
    }

    @PostMapping("/secretaire")
    public Secretaire postMembre(@RequestBody Secretaire s) {
        return this.membreMetier.creerSecretaire(s);
    }

    @PostMapping("/enseignant")
    public Enseignant postMembre(@RequestBody Enseignant e) {
        return this.membreMetier.creerEnseignant(e);
    }

    @PostMapping("/president")
    public President postMembre(@RequestBody President p) {
        return this.membreMetier.creerPresident(p);
    }

}
