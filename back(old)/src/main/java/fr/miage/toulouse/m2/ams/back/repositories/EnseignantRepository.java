package fr.miage.toulouse.m2.ams.back.repositories;

import fr.miage.toulouse.m2.ams.back.entities.Enseignant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnseignantRepository extends CrudRepository<Enseignant,Long> {
}
