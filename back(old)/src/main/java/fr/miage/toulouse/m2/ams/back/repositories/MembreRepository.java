package fr.miage.toulouse.m2.ams.back.repositories;

import fr.miage.toulouse.m2.ams.back.entities.Membre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MembreRepository extends CrudRepository<Membre,Long> {
}
