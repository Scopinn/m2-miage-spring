package fr.miage.toulouse.m2.ams.back.services;

import fr.miage.toulouse.m2.ams.back.entities.*;

public interface MembreMetier {

    public Membre creerMembre(Membre m);

    public Secretaire creerSecretaire( Secretaire s);

    public Enseignant creerEnseignant( Enseignant e);

    public President creerPresident( President p);


}
