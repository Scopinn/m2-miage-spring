package fr.miage.UtilisateursCours;

import fr.miage.UtilisateursCours.repository.CoursDetailsRepository;
import fr.miage.UtilisateursCours.repository.CoursDetailsRepositoryImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
public class UtilisateurCoursApplication {

	public static final String USER_SERVICE_URL = "http://USERS-SERVICE";

	public static final String CLASS_SERVICE_URL = "http://CLASSES-SERVICE";

	public static void main(String[] args) {
		SpringApplication.run(UtilisateurCoursApplication.class, args);
	}

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public CoursDetailsRepository userWithClassRepository() {
		return new CoursDetailsRepositoryImpl(USER_SERVICE_URL, CLASS_SERVICE_URL);
	}
}
