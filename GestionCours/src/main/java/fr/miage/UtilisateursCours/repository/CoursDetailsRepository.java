package fr.miage.UtilisateursCours.repository;

import fr.miage.UtilisateursCours.transientobj.ClassDetails;

public interface CoursDetailsRepository {

    ClassDetails getClassDetails(Long idClass) throws Exception;

}
