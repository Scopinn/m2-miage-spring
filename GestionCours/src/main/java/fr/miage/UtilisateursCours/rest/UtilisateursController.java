package fr.miage.UtilisateursCours.rest;

import fr.miage.UtilisateursCours.repository.CoursDetailsRepository;
import fr.miage.UtilisateursCours.transientobj.ClassDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController("/")
public class UtilisateursController {

    @Autowired
    CoursDetailsRepository CoursDetailsRepository;

    @GetMapping("{id}")
    public ClassDetails getClassDetails(@PathVariable("id") Long id) {
        try {
            return this.CoursDetailsRepository.getClassDetails(id);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    /*@PostMapping("")
    public ClassDetails post(@RequestBody User user) {
        try {
            return this.userWithClassRepository.save(user);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }*/
}
