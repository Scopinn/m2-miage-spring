package fr.miage.gestionUtilisateur.repositories;

import fr.miage.gestionUtilisateur.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurRepository extends CrudRepository<User, Long> {

    User findOneByEmail(String email);

    User findOneByLogin(String login);
}
