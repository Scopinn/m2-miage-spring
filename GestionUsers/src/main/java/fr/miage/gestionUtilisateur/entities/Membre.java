package fr.miage.gestionUtilisateur.entities;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Membre {

    @Id
    public Long id;

    @NonNull
    public String nom;
    @NonNull
    public String prenom;
    @NonNull
    public String adresseMail;
    @JoinColumn(name = "identifiantId", referencedColumnName = "id")
    @NonNull
    public String identifiant;
    @NonNull
    public String motDePasse;
    @NonNull
    public String adresseResidence;
    @NonNull
    public String niveauExpertise;


}
